T=1/30;

x=[0.3 1 0.5 0.7];
tx = -1:2;

t=-5:T:5;

u = (2*min(t)):T:(2*max(t));
good = find(abs(u) <= 5);

rect_1 = (abs(t) < 1/2);

rect_2 = conv(rect_1, rect_1)*T;
rect_2 = rect_2(good);

rect_3 = conv(rect_1, rect_2)*T;
rect_3 = rect_3(good);

rect_4 = conv(rect_2, rect_2)*T;
rect_4 = rect_4(good);

Rects=[rect_1 ; rect_2; rect_3; rect_4];

colors = {'b', 'r', 'k', 'm'};
stili = {'-', '--', '-', '--'};

figure(1)
clf

x_range=3*[-1, 1];
h=[];

for k=1:4
  subplot(2,2,k)

  y = Rects(k,:);
  color_idx=1;

  for n=(x_range(1)):(x_range(2)-1)
    I = [n, n+1];

    if mod(k,2)==1
      I=I+1/2;
    end

    pos = find((t >= I(1)) .* (t <= I(2)));

    if any(y(pos) > 0)
      plot(t(pos), y(pos), ...
	   [colors{color_idx}, stili{color_idx}], ...
	   'linewidth', 2)
      
      color_idx = color_idx + 1;
      hold on
    end
  end
  
  ylim([0, 1.2])
  xlim(x_range)
  grid on
  
  h=title(sprintf('rect^{*%d}', k));
  set(h, 'fontsize', 2*get(h, 'fontsize'))
  
  set(gca, 'fontsize', 2*get(gca, 'fontsize'))
end

set(gcf, 'position', get(gcf, 'position')*diag([1 1 2 2]))

print -dpng 4-finestre.png

figure(2)
clf

stili = {'-', '--', '-.', ':'};

for k=1:4
  [RR, w]=freqz(Rects(k,:)*T, 1, [], 1/T);
  plot(w, 20*log10(abs(RR)), ...
       [colors{k}, stili{k}], ...
       'linewidth', 2)
  
  hold on
end

ylim([-100 0])
set(gca, 'ytick', -100:10:0)
xlim([0 10])
ylabel('dB')
xlabel('frequency (a.u.)')
grid on

h=legend('rect', 'rect^{*2}', 'rect^{*3}', 'rect^{*4}')
set(h, 'fontsize', 3*get(h, 'fontsize'))

set(gca, 'fontsize', 2*get(gca, 'fontsize'))

set(gcf, 'position', get(gcf, 'position')*diag([1 1 2 2]))

print -dpng 4-finestre-freq.png
