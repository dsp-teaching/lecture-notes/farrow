T=1/30;

x=[0.3 1 0.5 0.7];
tx = -1:2;

t=-5:T:5;

u = (2*min(t)):T:(2*max(t));
good = find(abs(u) <= 5);

rect_1 = (abs(t) < 1/2);

rect_2 = conv(rect_1, rect_1)*T;
rect_2 = rect_2(good);


rect_4 = conv(rect_2, rect_2)*T;
rect_4 = rect_4(good);

good = find(abs(t) <=2);
rect_4 = rect_4(good);
t = -2:T:2;

figure(1)
clf

subplot(2,1,1)
plot(t, rect_4);
hold on

u = [find(t==-1), find(t==0), find(t==1)];

q0 = rect_4(u);
stem(-1:1, q0);

grid on
xlabel('(a)')

subplot(2,1,2)
zplane(1, q0)
xlabel('(b)')

% print -dpng rect4-e-poli.png

close(2)
figure(2)

poles = roots(q0);
[~,k]=min(abs(poles));
p = poles(k)

n=-7:7;
stem(n, p .^ abs(n), 'linewidth', 1.5)

grid on
xlabel('n')
ylabel('Impulse response')

set(gca, 'fontsize', 2*get(gca, 'fontsize'))

set(gcf, 'position', get(gcf, 'position')*diag([1 1 2 2]))

print -dpng impulse-response.png
