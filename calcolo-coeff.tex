% -*- mode: latex; eval: (electric-indent-mode 0);
\documentclass[a4paper]{article}
\usepackage[export]{adjustbox}
\usepackage{setspace}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amsbsy}
\usepackage{amssymb}
\usepackage{amsthm,amsfonts}
\usepackage{epsf,epsfig}
\usepackage{times}
\usepackage{mathptmx}
\usepackage{color}
\usepackage{nomelabel}
\usepackage[english]{babel}
\usepackage{srcltx}
\usepackage[boxed]{algorithm2e}
\usepackage{ifpdf}
\usepackage[colorlinks=true,
  linkcolor=blue,
  filecolor=blue,
  citecolor=blue]{hyperref}
\usepackage{hyperxmp}
\usepackage[type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}
\usepackage{bbm}
\input{notes-def}



\newcommand{\invz}{\optpara{\xxxinvz}{1}}
\newcommand{\xxxinvz}[1]{z^{-#1}}
\newcommand{\bshift}[1]{b_{#1}}
\newcommand{\prectiv}[1][\epsilon]{r^{(4)}_{#1}}
\newcommand{\xd}[1][\epsilon]{x_{#1}}
\newcommand{\sincd}[1][\epsilon]{\sinc_{#1}}
\newcommand{\trid}[1][\epsilon]{\tri_{#1}}
\newcommand{\xc}{x^{(c)}}
\newcommand{\xclin}{\xc_{\text{lin}}}
\newcommand{\xciv}{\xc_{\rect^4}}
\newcommand{\hc}{h^{(c)}}
\newcommand{\prect}[1]{\rect^{*#1}}
\newcommand{\I}[2]{I_{#1,#2}}
\newcommand{\V}[1]{{\mathcal V}_{#1}}
\newcommand{\fracup}[1]{\Delta\parasenondot{#1}}

\title{Computing the Farrow filter coefficients}

\author{Riccardo Bernardini}

\begin{document}
\maketitle

\begin{abstract}
  Computing the coefficients of the four filters $Q_k$, $k=0, 1, 2,
  3$, entering the Farrow scheme can be a bit confusing at first.  The
  goal of this brief document is to try to disentangle the matter.
\end{abstract}


The coefficients of filter $r_\epsilon : \interi \to \reali$ are defined as
\begin{equation}
  \label{eq:1}
  r_\epsilon(n) = \prect4(n-\epsilon)
\end{equation}
%
Function $\prect4 : \reali \to \reali$ is piecewise cubic in the sense that
for every $n\in\interi$ and $t \in (n-1, n)$ it holds
%
\begin{equation}
  \label{eq:2}
  \prect4(t) = S_n(t), \qquad t \in (n-1, n)
\end{equation}
%
where $S_n$ is a third degree polynomial, that is,
%
\begin{equation}
  \label{eq:3}
  S_n(t) = \sum_{k=0}^3 b_{n,k} t^k = b_{n,0} + b_{n,1} t + b_{n,2} t^2 + b_{n,3} t^3 
\end{equation}
%
for some coefficients $b_{n,0}$, $b_{n,0}$, $b_{n,2}$ and $b_{n,3}$.

\begin{commento}
   Polynomials $S_n$ can be easily obtained (although it is a bit
   boring) by computing the convolution
%
\begin{equation}
  \label{eq:conv}
  S_n(t) = \prect4(t) = \int_\reali \tri(t-\tau) \tri(\tau) d\tau,
  \qquad t \in (n-1, n)
\end{equation}
%
Note that since the support of $\tri$ is $(-1,1)$, only $S_{-1}$,
$S_0$, $S_1$ and $S_2$ are not zero; moreover, since $\prect4$ is even
it is easy to see that
%
\begin{equation}
  S_n(t) = S_{1-n}(-t)
\end{equation}
%
Therefore, only two integrals \er{eq:conv} need to be computed.
\end{commento}

Consider now equation \er{eq:1}.  Clearly, since $\epsilon
\in (0,1)$, $n-\epsilon \in (n-1, n)$ and using \er{eq:2} we obtain
%
\begin{equation}
  \label{eq:4}
  r_\epsilon(n) = \prect4(n-\epsilon) = S_n(n-\epsilon) =
  \sum_{k=0}^3 b_{n,k} (n-\epsilon)^k
\end{equation}
%
Now to expand $(n-\epsilon)^k$ as
%
\begin{equation}
  \label{eq:power}
  (n-\epsilon)^k = \sum_{\ell=0}^3 \binom{k}{\ell} \epsilon^\ell n^{k-\ell}
\end{equation}
%
where we assume, for notational convenience, $\binom k\ell=0$ if $\ell
> k$ (in this way we can keep the sum running between 0 and 3 rather
than between $0$ and $k$).  By using \er{eq:power} in \er{eq:4} one obtains
%
\begin{equation}
  r_\epsilon(n) =   \sum_{k=0}^3 b_{n,k} \sum_{\ell=0}^3
  \binom{k}{\ell} \epsilon^\ell n^{k-\ell} 
\end{equation}
%
and by swapping the two sums
%
\begin{equation}
  \label{eq:qq}
  r_\epsilon(n) = \sum_{\ell=0}^3 \epsilon^\ell \commenta{\left[ \sum_{k=0}^3 b_{n,k} 
  \binom{k}{\ell}  n^{k-\ell}\right]}{c_{n,\ell}}
\end{equation}
%
Equation \er{eq:qq} shows that 
$r_\epsilon(n)$ can be written as a third degree polynomial in
$\epsilon$, that is, 
%
\begin{equation}
  r_\epsilon(n) =   \sum_{\ell=0}^3 c_{n,\ell} \epsilon^\ell
\end{equation}
%
The z-transform of filter $r_\epsilon$ is
%
\begin{equation}
  R_\epsilon(z) = \sum_{n\in\interi} r_\epsilon(n) z^{-n}
  = \sum_{n\in\interi} \sum_{\ell=0}^3 c_{n,\ell} \epsilon^\ell z^{-n}
  = \sum_{\ell=0}^3 \epsilon^\ell \commenta{\sum_{n\in\interi}
    c_{n,\ell}  z^{-n}}{Q_\ell(z)} 
\end{equation}

\begin{commento}
  Note that the polynomials $S_n$ \textbf{are not} the polynomials
  $P_n$ in the main document. It is not too difficult to check that
  $P_n(\epsilon) = S_n(n-\epsilon)$.
\end{commento}


\end{document}


% LocalWords:  oversample oversampled samp interpolator Nyquist sinc
% LocalWords:  eps BIBO lineare rects rect trid lin fourier sintesi
% LocalWords:  nyquist equ IIR poli
