#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 SVG-filename"
    exit 1
fi

src=$1
dst=`basename $1 .svg`.png

inkscape "--export-filename=$dst"        \
	 --export-type=png               \
	 --export-background-opacity=255 \
	 "$src"
