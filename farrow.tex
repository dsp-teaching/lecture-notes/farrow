% -*- mode: latex; eval: (electric-indent-mode 0);
\documentclass[a4paper]{article}
\usepackage[export]{adjustbox}
\usepackage{setspace}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amsbsy}
\usepackage{amssymb}
\usepackage{amsthm,amsfonts}
\usepackage{epsf,epsfig}
\usepackage{times}
\usepackage{mathptmx}
\usepackage{color}
\usepackage{nomelabel}
\usepackage[english]{babel}
\usepackage{srcltx}
\usepackage[boxed]{algorithm2e}
\usepackage{ifpdf}
\usepackage[colorlinks=true,
  linkcolor=blue,
  filecolor=blue,
  citecolor=blue]{hyperref}
\usepackage{hyperxmp}
\usepackage[type={CC},
  modifier={by-sa},
  version={4.0},
]{doclicense}
\usepackage{bbm}
\input{notes-def}



\newcommand{\invz}{\optpara{\xxxinvz}{1}}
\newcommand{\xxxinvz}[1]{z^{-#1}}
\newcommand{\bshift}[1]{b_{#1}}
\newcommand{\prectiv}[1][\epsilon]{r^{(4)}_{#1}}
\newcommand{\xd}[1][\epsilon]{x_{#1}}
\newcommand{\sincd}[1][\epsilon]{\sinc_{#1}}
\newcommand{\trid}[1][\epsilon]{\tri_{#1}}
\newcommand{\xc}{x^{(c)}}
\newcommand{\xclin}{\xc_{\text{lin}}}
\newcommand{\xciv}{\xc_{\rect^4}}
\newcommand{\hc}{h^{(c)}}
\newcommand{\prect}[1]{\rect^{*#1}}
\newcommand{\I}[2]{I_{#1,#2}}
\newcommand{\V}[1]{{\mathcal V}_{#1}}
\newcommand{\fracup}[1]{\Delta\parasenondot{#1}}

\title{Fractional delays}

\author{Riccardo Bernardini}

\begin{document}
\maketitle

\section{Introduction}

Consider a discrete-time signal $x : \interi \to \complessi$ obtained
by sampling a continuous time signal $\xc : \reali \to \complessi$ at  frequency $F_c$ (and sampling period $T=1/F_c$).  If you want to
create a signal $\xd[m]:\interi\to \complessi$ that is the translated
version of $x$ by an integer number of step, say $m \in \interi$, it
suffices to define $\xd[m](n)=x(n-m)$. In an hardware implementation it
suffices to use a chain with $m$ delay elements.

But what if we want to translate $x$ of a fractional amount $\epsilon
\in (0,1)$?  For sure we cannot use $\xd(n)=x(n-\epsilon)$: the right
hand side does not even make sense.  Therefore, before searching for
an algorithm we need to find a precise sens to ``translate $x$ by
$\epsilon$,'' but, more importantly, we need to answer the question
``\textbf{why} do we want to translate $x$ by $\epsilon$?''  Actually,
answering to the latter question will help us to answer the former.

\paragraph{Notation}
Before beginning, it is worth defining some notations that will prove
useful.
\begin{itemize}
\item 
  We will often an upperscript $(c)$, as in $\xc$, to denote a
  continuous time signal, that is a function $\xc :\reali \to
  \complessi$.  We will often use this notation when $\xc$ is related
  (in a sense that depends on the context) to a discrete time signal
  $x$.
\item
  If $x$ is any (continuous or discrete time) signal, we will denote its
  translation by $d$ as $\tau^d x$, that is,  $\tau^d x(t)=x(t-d)$.
\item
  If $u\in\reali$ we will denote with $\ceil u$ the smallest integer
  such that $\ceil u \geq u$; in a sense, $\ceil u$ is $u$ rounded up.
  We will also define $\fracup u=\ceil u-u$ that represents, in a
  sense, the fractional part of $u$ when rounded up.  Observe that
  $0 \leq \fracup u < 1$
\item
  We define the function $\rect:\reali\to\reali$ as
  \begin{equation}
  \rect(t) = 
  \begin{cases}
    1 & \abs t < 1/2\\
    0 & \text{else}
  \end{cases}
\end{equation}
\item If $x$ is any (continuos or discrete) time signal, we will
  denote with $x^{*k}$ the result of convolving $k$ copies of $x$,
  that is,
  \begin{equation}
    \label{eq:stark}
  x^{*k} = \commenta{x * x * \cdots * x}{\text{$k$
      copies of $x$}}
\end{equation}
\end{itemize}

\subsection{Why?}

There are a few applications where being able to apply a fractional
translation to a discrete time signal can prove useful.

\paragraph{Sampling epoch adjustment}
In many schemes of digital demodulation it is crucial that the
receiver is synchronized with the transmitter and that they have the
same \emph{sampling epoch} (roughly speaking, the instant corresponding to
$t=0$).  The receiver can use several approaches to estimate the
difference in the epochs, but then there is the problem of changing
the sampling times in order to adapt them.  One can devise a few
different solutions, for example
\begin{description}
  \item [Oversampling] One can oversample the input signal at a
    frequency $N F_c$ and then decimate the result by a factor $N$.
    By translating the oversampled signal one can simulate a change of
    epoch, of course with the resolution of $T/N$.  This technique is
    quite simple, but if $F_c$ is large it could be not feasible.
  \item [Hardware] One can imagine an hardware device that can shift
    the clock controlling the ADC; for example, a counter that sends
    an impulse to the ADC every time the counter reach a given
    ``threshold'' value, settable from software.  By changing the
    threshold, the sampling epoch can be adjusted.  This solution does
    not require oversampling, but additional hardware.
  \item [Software] It would be nice to have an algorithm that,
    starting from, gives us $\xd$ which is \emph{the signal we would
    had obtained if we sampled the continuous time signal $\xc$ with
    epoch $\epsilon$}
\end{description}

\paragraph{Change of sampling frequency and irregular interpolation}

Suppose you have a signal $x_1$ sampled at frequency $F_1$ and you want
to change the sampling frequency to $F_2$.  This is quite easy if
$F_2/F_1=\alpha=p/q$ with $p$ and $q$ integer, mutually prime and ``small;''
in this case it suffices to interpolate $x$ by a factor $p$, filter
with a suitable low-pass filters and decimate by a factor $q$ the
result.

The problem is more complex if $p$ and $q$ are not ``small'' (e.g.,
$p=181$ and $q=293$) or if $\alpha$ is not even rational.  In this
case
\begin{equation}
  \label{eq:interp}
  \begin{split}
    x_2(n) &= \xc(n F_2) \\
    &= \xc(n F_1 \alpha) \\
    &= x_1(n\alpha) \\
    &= x_1(\ceil
  {n\alpha} - \fracup{n\alpha})\\
    &= x_{1,\fracup{n\alpha}}(\ceil  {n\alpha})
  \end{split}
\end{equation}
%
Rememeber that we defined $\fracup s$ as $\fracup s=\ceil{s}-s$.  According to \er{eq:interp}, $x_2(n)$ is the
$\floor{n\alpha}$-th sample of $x_{1,\fracup{n\alpha}}$, that is, $x_1$
translated by $\fracup{n\alpha}  \in
(0,1)$.  Clearly, an algorithm for fractional
translation would provide an efficient way to change the sampling
frequency.

\subsection{Meaning of \sout{~life~} fractional delay}

With the examples above, we can give a precise (more or less) meaning
to ``translate $x$ of $\epsilon$.''  It is worth first to give a name
to a continuous time signal that gives $x$ when sampled.
First we introduce some useful nomenclature.

\begin{definition}
  \label{def:padre}
  Let $x:\interi \to \complessi$ be a discrete time signal.  We will
  say that the continuous time signal $\xc :\reali \to \complessi$ is
  a \emph{continuous time progenitor} of $x$ if
  %
  \begin{equation}
    \label{eq:compat}
    x(n) = \xc(n), \qquad \forall n \in\interi
  \end{equation}
  %
  that is, if sampling $\xc$ (with $T=1$) gives $x$.
\end{definition}

Now we can give a precise meaning (almost) to the phrase  ``translate
$x$ of $\epsilon$.''
%
\begin{definition}
  \label{def:tau}
  Let $x:\interi \to \complessi$ be a discrete time signal and let
  $\xc :\reali \to \complessi$ be a \emph{continuous time progenitor}
  of $x$.  The \emph{translation of $x$ by $\epsilon\in \reali$} is
  defined as the signal that one would obtain by translating the
  progenitor $\xc$ before sampling, that is,
  %
  \begin{equation}
    \xd(n) = [\tau^\epsilon \xc](n) = \xc(n-\epsilon)
  \end{equation}
\end{definition}

Well, at least now we know what are searching for. (Really?)

\section{Progenitors \& algorithms}

Definition~\ref{def:tau} has subtle issue: it depends on the specific
progenitor $\xc$. Clearly, unless some specific hypothesis are done
about $\xc$ (e.g., $\xc$ is band limited), there is an infinite numbers
of progenitors of $x$ and every progenitor gives rise to a different
$\xd$.  Therefore, we need to make some hypothesis about $\xc$ to make
it unique. Well, the most ``natural'' way is to use the Sampling
Theorem and suppose that $\xc$ was band limited.  Before exploring
this solution, it is convenient to discuss in a general context
schemes like the one in \fref{int-f-samp}a that will return often in
this document.

\subsection{Preliminary remarks}
\label{ss:pre}
Consider \fref{int-f-samp}a that shows a cascade of an interpolator
(the block with the upward arrow), a filter with impulse response
$\hc$ and a sampler (the block with the downward arrow).  The
input-output relation of the interpolator is
%
\begin{equation}
  \label{eq:int0}
  u(t) = \sum_{n\in\interi} \delta(t-n) x(n) \qquad  t\in\reali
\end{equation}
%
that is, the output of the interpolator is a sequence of Dirac deltas
centered on integer instants and with the amplitudes given by the
samples of the input signal $x$.  The input-output relation of the
sampler is, clearly,
\begin{equation}
  y(n) = y_c(n), \qquad n\in\interi
\end{equation}
%
that is, the output is a discrete time signal $y : \interi \to
\complessi$ equal to the input $y_c :\reali\to\complessi$ at integer
times.

By taking the convolution of $\hc$ with \er{eq:int0} one obtains,
with reference to \fref{int-f-samp}a
%
\begin{equation}
  y_c = \sum_{n\in\interi} x(n) \tau^n \hc
\end{equation}



\begin{figure}
  \unafigura{int-e-samp}  
  \caption{(a) Interpolator-filter-sampler cascade (b) Discrete time
    equivalent scheme \label{fig:int-f-samp}}
\end{figure}

The proof of the following lemma is immediate.

\begin{lemma}
  \label{lem}
  The scheme of \fref{int-f-samp}a is equivalent to the scheme of
  \fref{int-f-samp}b, where $h:\interi \to \complessi$ is the sampled
  version of $\hc$, that is,
  \begin{equation}
    h(n) =\hc(n), \qquad n\in\interi
  \end{equation}
  %
  Moreover, if $x \in\elldue\interi$ and $\hc \in\Elldue\reali$,
  signal $y_c : \reali \to \complessi$ belongs to $\V\hc$, the
  subspace of $\Elldue\reali$ defined as
  %
  \begin{equation}
    \V\hc = \vsspan \{ \tau^n \hc, \;\; n\in\interi\}
  \end{equation}
  %
  Space $\V\hc$ is the space generated by $\hc$ and its integer
  translations. 
\end{lemma}

\subsection{First tentative: $\sinc$}

The most natural hypothesis for $\xc$ is to suppose that $\xc$ is
band-limited.  Nyquist theorem grants us that the band-limited
progenitor is unique and that it can be reconstructed by interpolating
$x$ with a $\sinc$, as shown in \fref{sinc-e-eps}a.  After
reconstructing $\xc$, according to Definition~\ref{def:tau}, we
translate $\xc$ by $\epsilon$ and sample the result, as shown in
\fref{sinc-e-eps}a.

Since the low-pass and the continuous-time translation are both
filters, they can be combined in a single filter whose impulse
response is $\tau^\epsilon \sinc$, as shown in \fref{sinc-e-eps}b.
Now it suffices to apply Lemma~\ref{lem} to obtain the scheme in
\fref{sinc-e-eps}c where
%
\begin{equation}
  \sincd(n) = \sinc(n-\epsilon)
\end{equation}
%
Therefore, in order to obtain $\xd$ it suffices to filter $x$ with
$\sincd$. Easy, right? Not so fast\ldots


\begin{figure}
  \begin{center}
\unafigura{sinc-e-epsilon}    
  \end{center}
  \caption{Generating fractional delays with a band-limited
    progenitor. (a) Original scheme (b) Merging together low-pass
    filtering and translation (c) Applying Lemma~\ref{lem}
    schemes. \label{fig:sinc-e-eps}}
\end{figure}


\subsubsection{What is wrong with the $\sinc$?}
\label{ss:sinc}

The result above is nice on paper, but it has several practical issues

\begin{enumerate}
\item $\sincd$ has not a rational transfer function, making it
  impossible to use finite memory filter. Actually, it has
  \textbf{no} transfer function, since it is not BIBO stable.
\item Since it decays as $1/n$, if you want to approximate it with a
  FIR filter, you need to take quite a large number of samples to get
  a good approximation.
\item Every time you want to change $\epsilon$ you need to recompute
  the impulse response from scratch and this can be quite inconvenient.
\end{enumerate}

\subsection{Second tentative: linear interpolation}
\label{ss:tri}
We can try to change our ``progenitor model.'' Indeed, the band
limited hypothesis it was just an hypothesis that allowed us to
reconstruct $\xc$ from its samples $x$.  Sure, it was a ``natural''
hypothesis since we could assume that $x$ was ``correctly sampled,''
that is, satisfying the conditions of the sampling theorem.  However,
strictly band limited signals are just an idealization, a nice
approximation that makes the theory simpler.  This suggests that there
is nothing sacred in the band limited hypothesis and we can choose a
different progenitor model, as long as the obtained $\xc$ is
reasonable.

\begin{figure}
  \centering
  \unafigura{lineare-a-tratti}
  \caption{Piece-wise linear progenitor \label{fig:linear}}
\end{figure}

We begin exploring this approach using a simple model: we require
that $\xc$ is a piece-wise linear signal with ``breaking points''
corresponding to integer times.  We can express this analytically as
(see also \fref{linear})
%
\begin{equation}
  \label{eq:intlin}
  \xclin(t) = x(n) + (t-n) [x(n+1)-x(n)], \qquad t \in (n, n+1)
\end{equation}
%
\begin{commento}
One could object that this is an approximation quite rough and that
the signal in \fref{linear} does not look ``natural'' at all, but this
is just a first tentative, useful to gain some intuition.
\end{commento}
%
It is well known that $\xclin$ in \er{eq:intlin} can be obtained by
interpolating $x$ with the filter with impulse response
\begin{equation}
  \tri(t) = \prect 2(t) =
  \begin{cases}
    0 & \abs t > 1 \\
    1-\abs{t} & \abs t \leq 1
  \end{cases}
\end{equation}
%
Remember that, according to definition \er{eq:stark}, $\prect 2 =
\rect * \rect$. 

By a reasoning exactly identical to the one used in
Section~\ref{ss:sinc} (where no specific properties of $\sinc$ were
used) we can deduce that if we choose \er{eq:intlin} as progenitor,
one can obtain $\xd$ by computing the convolution of  $x$ with $\trid : \interi \to
\complessi$ obtained by sampling $\tau^\epsilon \tri$, that is
%
\begin{equation}
  \trid(n) = \tau^\epsilon \tri(n)=\tri(n-\epsilon), \qquad n\in\interi
\end{equation}
%
We can assume that $\epsilon \in (0,1)$ since every translation by an
integer can be done as a normal discrete-time translation.  If we
assume $\epsilon \in (0,1)$, it is easy to check, even with the help
of \fref{tri-e-epsilon}, that $\trid(n)\ne 0$ only for $n=0$ and $n=1$,
more precisely
%
\begin{equation}
  \trid(n) = 
  \begin{cases}
    \tri(-\epsilon) = 1-\epsilon & n=0 \\
    \tri(1-\epsilon) = \epsilon & n=1 \\
    0 & \text{else}
  \end{cases}
\end{equation}
%
It follows that $\trid$ is a FIR filter with transfer function
%
\begin{equation}
  \label{eq:Te}
  T_\epsilon(z) = 1-\epsilon + \epsilon\invz =
  1 + \epsilon(\invz -1)
\end{equation}
%
and flow diagram show in \fref{trid}. It is easy to recognize that
from \er{eq:Te} we can recover  the interpolation formula
\er{eq:intlin}.

\begin{figure}
  \begin{center}
    \unafigura{tri-e-epsilon}
  \end{center}
  \caption{Sampling the translated version of the
    triangle \label{fig:tri-e-epsilon}}
\end{figure}

\begin{figure}
  \begin{center}
    \unafiguray[3cm]{raw-lin}
  \end{center}
  \caption{Flow diagram for fractional delays with piece-wise linear
    progenitor \label{fig:trid}}
\end{figure}

A nice feature of this solution is its efficiency: just two sums and a
product by $\epsilon$.  Another interesting feature is that we have a
simple and direct control over the translation since it suffices to
change a single coefficient; compare this with the need of recomputing
the filter with the band-limited model.

The main problem with this solution is that a piece-wise linear
progenitor is not very ``natural.''  You can see this also in the
frequency domain: the Fourier transform of a signal that has
``corners'' (continuous, but not differentiable) decays as $1/f^2$ in
frequency.  Even if we give up with the strictly band limited model,
it would be nice to have a progenitor that is ``fairly band limited,''
therefore it would be nice if the Fourier transform decayed faster
than just $1/f^2$.  This means that we would like a smoother
progenitor, maybe a few times differentiable.

\subsection{Third (and last) tentative: piece-wise polynomial}

The problem with piece-wise linear signals is that every segment has
only two degrees of freedom (slope and intercept) that we use to fix
its extremes.  If we want more smoothness (e.g., equal derivatives at
the border between two adjacent intervals) we need more degrees of
freedom.  This suggests to use polynomials with larger degrees.

Every piece-wise linear function can be obtained as linear
combinations of the $\tri$ and its integer translations.  This
suggests to use a similar approach, replacing the $\tri$ with
something that is piece-wise polynomial of larger degree and smooth.
An easy way to construct the $\tri$ replacement is to convolve $\tri$
with $\rect$, every convolution increase the smoothness and the
polynomial degree.  This suggests the following definition

\begin{definition}
  Let $k \in\naturali$, we define $V_k$ as the subspace of $\Elldue
  \reali$ generated by the integer translations of $\prect{k+1}$, that
  is
  \begin{equation}
    \label{eq:vk}
    V_k = \vsspan\{\tau^n\prect{k+1}, \quad n\in\interi\}
  \end{equation}
\end{definition}
%
Note that $V_1$ is the space generated by the integer translation of
the triangle $\tri=\prect 2$, that is, $V_1$ is the space of piece-wise linear
functions considered in Section~\ref{ss:tri}.  Intuitively, spaces
$V_k$, $k > 1$, generalize the concept of $V_1$ and since every new
convolution improve the smoothness, we expect the function in $V_2$ to
be smoother than the function in $V_1$ and the functions in $V_3$
smoother than those in $V_2$ and so on, as claimed by the following
property.

% \begin{definition}
%   We will denote with $\I k n$, $k,n\in\interi$, the interval
%   \begin{equation}
%     \I k n = 
%     \begin{cases}
%       (n-1/2, n+1/2) & \text{if $k$ is even} \\
%       (n, n+1) & \text{if $k$ is odd} 
%     \end{cases}
%   \end{equation}
% \end{definition}
% %
% Every interval $\I kn$ has unitary length, but it is centered on $n$
% if $k$ is even, while it starts from $n$ if $k$ is odd.   This
% peculiar notation will prove useful in the following.

\begin{property}
  Every function $u \in V_k$ enjoys the following properties
  \begin{itemize}
  \item Function $u$ is $k-1$ times derivable, where with \emph{0 times
  derivable} ($k=1$) we mean that the functions are continuous, but not
    necessarily differentiable, while with \emph{-1 times
    differentiable} ($k=0$) we mean that the function can be discontinuous.
  \item It is piece-wise polynomial of degree $k$.  More precisely,
    for every $n\in\interi$, the restriction of $u$ to $I_{n,k}= (n,
    n+1)-\bshift k$ where
    %
    \begin{equation}
      \bshift k = 
      \begin{cases}
        0 & \text{$k$ odd} \\
        1/2 & \text{$k$ even} 
      \end{cases}
    \end{equation}
    %
    coincides with a polynomial of degree at most $k$, see \fref{rects}a.
  \item The Fourier transform $U(f)$ of $u$ decays not slower than
    $1/f^{k+1}$ when $f \to\infty$, see \fref{rects}b.
  \item It can be obtained using the scheme \fref{sintesi}, that is,
    by ``interpolating'' with filter $\prect{k+1}$ a suitable discrete
    time signal $c:\interi\to \complessi$.  Note that the samples of
    $c$ are the components of $u$ with respect to the basis in
    \er{eq:vk}.  In a sense, $V_k$ is the space of functions that can
    be generated using the scheme in \fref{sintesi}.
  \end{itemize}
\end{property}

\begin{figure}
  \begin{center}
    \begin{tabular}{c}
      \unafigura{4-finestre} \\ (a) \\
      \unafigura{4-finestre-freq} \\ (b) 
    \end{tabular}
  \end{center}
  \caption{(a) The first four ``convolutional powers'' of rect. For
    clarity, different polynomial pieces are plotted with different
    lines styles. (b) Comparison between their Fourier
    transforms. \label{fig:rects}}
  
\end{figure}

\begin{figure}
  \begin{center}
    \begin{tabular}{c}
      \unafiguray[1.2cm]{sintesi} \\ (a) \\[2em]
      \unafiguray[1.2cm]{prova-unicita} \\ (b)
    \end{tabular}
  \end{center}
  \caption{(a) Synthesis of a signal in $V_k$ by interpolation. (b)
    Used to prove the unicity of the progenitor where
    $r_0^{(k+1)}(n)=\prect{k+1}(n)$, $n\in\interi$, coherently with
    the notation used in the text. \label{fig:sintesi}}
\end{figure}

\begin{proof}
  We give just a sketch, leaving the details to the reader.  It
  suffices to prove that $\prect{k+1}$ satisfies the two properties
  above and that the same properties are preserved by integer
  translations (so that every $\tau^n \prect{k+1}$ satisfy them) and
  by linear combinations.
\end{proof}
%
Observe that

\begin{itemize}
\item $V_0$ contains functions generated by the translations of the
  $\rect$.  Those functions  are piece-wise constant (0 degree
  polynomial) over intervals centered on the integers because the rect
  has support $(-1/2, 1/2)$.  Clearly we
  cannot impose not even continuity on piece-wise constant functions
  (i.e., they are derivable -1 times)
\item $V_1$ contains functions generated by the translation of
  $\tri=\prect 2$.
  They are derivable 0 times (i.e., continuous) and piece-wise linear.
  This is where the progenitor considered in Section~\ref{ss:tri}
  belongs.
\item $V_2$ contains functions generated by the translations of
  $\prect 3$; they that are derivable 1 time and piece-wise
  parabolic.
\item $V_3$ contains functions generated by the translations of
  $\prect 4$; they are derivable 2 times and piece-wise
  cubic on intervals with integer extremes.
\item and so on\ldots
\end{itemize}
%
A commonly used space is $V_3$, generated by $\prect 4$ and its
translations and whose functions have Fourier transform that decays as
$1/f^4$.  Therefore, in the following we will use $\xciv$, the
progenitor of $x$ that belongs to $V_3$.

\begin{commento}[Existence and unicity of $\xciv$]
  The sentence above has a technical issue: in order to say \emph{the
  progenitor of $x$ that belongs to $V_3$} we should prove that (i)
  said progenitor exists and (ii) it is unique.  Existence will be
  costructively proved in the following, showing how to get $\xciv$.
  Unicity can be proved with the help of \fref{sintesi}b.

  It is easy to see that the progenitor in $V_3$ is not unique if and
  only if, with reference to \fref{sintesi}b, there is a \emph{non
  null} signal $c \in\elldue \interi$ that gives output $s=0$ when
  interpoled with $\prect {k+1}$ and then sampled.  Using again
  Lemma~\ref{lem}, this means that
  \begin{equation}
    \label{eq:tozero}
    c * r_0^{(k+1)} = 0
  \end{equation}
  %
  where $r_0^{(k+1)} : \interi\to\reali$ is defined as
  \begin{equation}
    r_0^{(k+1)}(n)=\prect{k+1}(n), \qquad n\in\interi.
  \end{equation}
  %
  However, since $\prect{k+1}$ has compact support, filter
  $r_0^{(k+1)}$ is FIR and this implies that no signal
  $c\in\elldue\interi$ can satisfy \er{eq:tozero}.
\end{commento}

In order to get $\xciv$, we use a scheme similar to the one used in
the piece-wise linear case, with the difference that instead of
interpolating with $\tri$, now we interpolate with $\prect 4$.
With a reasoning similar to the one used for the band-limited case and
the piece-wise linear case, we deduce that $\xd$ can be obtained by
taking the convolution of  $x$ with the filter
%
\begin{equation}
  \prectiv(n) = \tau^\epsilon \prect 4(n) = \prect 4(n-\epsilon)
\end{equation}
%
Since $\prect 4$ is piece-wise cubic, we can write (remembering
function $\fracup.$ defined in Section~\ref{ss:pre})
%
\begin{equation}
  \label{ab}
  \prect 4(t) = P_{\ceil t}(\Delta(t))
\end{equation}
%
where and every $P_n$, $n\in\interi$ is a polynomial of degree $3$, that
is,
\begin{equation}
  P_n(x) = \sum_{m=0}^3 c_{n,m} x^m
\end{equation}
%
for suitable coefficients $c_{n,m}$.  
Now we can write explicitly the impulse response $\prectiv$
\begin{equation}
  \label{eq:sampled}
  \prectiv(n) = \prect 4(n-\epsilon)=P_n(\epsilon) = \sum_{m=0}^3
  c_{n,m} \epsilon^m
\end{equation}
%
Since $P_n \ne 0$ only for $n \in \{-1, 0, 1, 2\}$, it follows that
$\prectiv(n) \ne 0 \implica -1 \leq n \leq 2$, that is, $\prectiv$ is
FIR.  The transfer function is
\begin{equation}
  \label{eq:para}
  \begin{split}
      R(z) &= \sum_{n=-1}^2 P_n(\epsilon) \invz:n\\
      &= \sum_{n=-1}^2 \sum_{m=0}^3  c_{n,m} \epsilon^m \invz:n  \\
      &=  \sum_{m=0}^3  \epsilon^m \commenta{\sum_{n=-1}^2 c_{n,m}  \invz:n}{Q_m(z)}  \\
      &=  \sum_{m=0}^3 \epsilon^m Q_m(z)
  \end{split}
\end{equation}
%
\fref{raw-r4} shows a flow diagram with a possible implementation of
$R(z)$.  Note how we have a single ``handle'' (the value of
$\epsilon$) allows us to change the amount of translation without
recomputing the filters.  

\begin{figure}
  \begin{center}
    \unafiguray[5cm]{raw-r4}
  \end{center}
  \caption{Implementation of $R(z)$ \label{fig:raw-r4}}
\end{figure}

\paragraph{We are not done yet\ldots}

Did you believe that the scheme of \fref{raw-r4} suffice?  Well it
does not.  In order to see that it cannot be right consider the case
$\epsilon=0$.  In this case the output of \fref{raw-r4} should be
equal to the input, but this is not true: if $\epsilon=0$, the scheme
of \fref{raw-r4} filters the input with $Q_0(z)$.

Why does this happen, but it did not happen with $\sinc$ and $\tri$?
The reason is shown in \fref{nyquist}: $\sinc$ and $\tri$ are
\emph{Nyquist pulses}, that is, they are equal to one in 0 and zero in
all the other integers; a consequence of this property is that by
interpolating a signal $x:\interi \to \complessi$ with $\sinc$ and
$\tri$ one obtains the progenitor of $x$.  The kernel $\prect 4$,
instead, is clearly not a Nyquist pulse; by interpolating $x$ with
$\prect 4$ one obtains a function in  $V_4$, but said function is not
the progenitor.

\begin{figure}
  \begin{center}
    \unafigura{nyq}
  \end{center}
  \caption{Sync and triangle are Nyquist pulses, $\rect^{*4}$ is
    not \label{fig:nyquist}}
\end{figure}

An alternative way to see this is to say that the components of the
progenitor of $x$ in $V_2$ (with respect to the basis given by $\tri$
and its translations) coincide with the samples of $x$, but in the
case of $V_4$ this is not true: by combining the translations of
$\prect 4$ using the samples of $x$ as coefficients we do not get the
progenitor of $x$.  We need to ``reprocess'' the samples of $x$ in
order to map it into the components of the progenitor.

\fref{equ} shows a graphical depiction of our problem: we are
searching for something suitable to put in the block labeled with
``?'' so that in \fref{equ} $y_c$ is the progenitor of $x$ which
happens if and only if $x=y$.  It is easy to check that the sequence
interpolator-$\prect 4$-sampler is equivalent to the filter $Q_0(z)$
in \er{eq:para} and \fref{raw-r4} whose impulse response is $\prect 4$
sampled in $\interi$ (consider \er{eq:sampled} with
$\epsilon=0$). Therefore, the question mark block can be replaced by a
filter IIR with transfer function $1/Q_0(z)$ and we get the final
scheme of \fref{equ-r4}.

\begin{figure}
  \begin{center}
    \unafigura{equalization}
  \end{center}
  \caption{Searching for an equalizer \label{fig:equ}}
\end{figure}

\begin{figure}
  \begin{center}
    \unafigura{equ-r4}
  \end{center}
  \caption{Final scheme with equalization \label{fig:equ-r4}}
\end{figure}

\paragraph{Nice, but we are not there yet\ldots}

Yes, there is subtle point that makes the scheme in \fref{equ-r4} not
practical.  The impulse response of $Q_0$ is the sampled version of
$\prect 4$.  As it can be seen from \fref{poli}a, $Q_0$ is FIR and it
has a symmetric impulse response and the inverse transfer function can
be written as
%
\begin{equation}
  \label{eq:invq}
  \frac{1}{Q_0(z)} = \frac{1}{c_1 z + c_0 + c_1 \invz}
  = \frac {c_1^{-1} \invz} {1 +(c_0/c_1) \invz + \invz:2}
\end{equation}
%
where $c_0=\prect 4(0)$ and $c_1=\prect 4(1)=\prect 4(-1)$.  Transfer
function \er{eq:invq} corresponds to the difference equation
%
\begin{equation}
  \label{eq:diffq}
  y(n) = -\frac{c_0}{c_1} y(n-1)-y(n-2) + \frac{1}{c_1} x(n-1)
\end{equation}

This means that $Q_0(z)=Q_0(\invz)$
and this implies that the zeros of $Q_0$ (that become the poles of
$1/Q_0$) are reciprocal.  Therefore, one zero of $Q_0$ (pole of
$1/Q_0$) must be larger than one.  This can be checked with the help
of \fref{poli}b which shows the poles of $1/Q_0$.

\begin{figure}
  \begin{center}
    \unafigura{rect4-e-poli}
  \end{center}
  \caption{(a) Impulse response of $Q_0$ obtained by sampling $\prect
    4$. (b) Pole positions of $1/Q_0$. \label{fig:poli}}
\end{figure}

This means that if the filter is implemented ``as usual,'' that is, by
setting the initial conditions as $y(-1)=y(-2)=0$ (which grants a
causal impulse response) we get an unstable filter.  In order to have
stability, we need the impulse response corresponding to the
region of convergence (ROC) that includes the unitary circle.  With this choice
of ROC the pole inside the unitary circle responds causally, while the
one outside the circle responds anticausally.  Since the two poles are
reciprocal, the impulse response, shown in \fref{imp-stabile} is
symmetrical.

\begin{figure}
  \begin{center}
    \unafiguray[5cm]{impulse-response}
  \end{center}
  \caption{Impulse response of the stable version of
    $1/Q_0(z)$ \label{fig:imp-stabile}}
\end{figure}

The problem with impulse response \fref{imp-stabile} is that it is of
infinite length on both sides, making it impossible tu use (unless the
computation is done off-line).  A possible solution is to truncate 
it to get a FIR.  Since the impulse response decays exponentially, the
number of samples we need to keep is usually small.

\end{document}


% LocalWords:  oversample oversampled samp interpolator Nyquist sinc
% LocalWords:  eps BIBO lineare rects rect trid lin fourier sintesi
% LocalWords:  nyquist equ IIR poli
