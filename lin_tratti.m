x=[1 3 -1 2 -0.5];
t=-1:3;

figure(1)
clf

stem(t, x);
hold on
plot(t, x, 'b', ...
     [-1.25 -1], [0.5 1], '-.b', ...
     [3 3.25],   [-0.5 0.1250], '-.b');

xlim([-2 4])
ylim([-2 4])
axis off

print -dsvg lineare-a-tratti.svg
