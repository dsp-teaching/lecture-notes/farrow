T=1/30;

x=[0.3 1 0.5 0.7];
tx = -1:2;

t=-5:T:5;

u = (2*min(t)):T:(2*max(t));
good = find(abs(u) <= 5);

rect_1 = (abs(t) < 1/2);

rect_2 = conv(rect_1, rect_1)*T;
rect_2 = rect_2(good);


rect_4 = conv(rect_2, rect_2)*T;
rect_4 = rect_4(good);

kernels=[sinc(t); rect_2; rect_4];

nomi = {'sinc', 'triangle', 'rect^{*4}'}

figure(1)
clf

x_range = 5*[-1, 1];

for k=1:3
  subplot(3,1,k)

  for m=1:length(tx)
    tau = tx(m);

    if tau == 0
      style='-k';
    else
      style='--b';
    end
    
    plot(t+tau, x(m)*kernels(k,:), style)
    hold on
  end

  stem(tx, x);
  xlim(x_range)
  set(gca, 'xtick', x_range(1):x_range(2))
  grid on
  title(nomi{k}, 'fontsize', 22)
end

set(1, 'position', [1 23 964 964])
print -dpng nyq.png
